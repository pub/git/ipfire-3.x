# One blacklist entry per line, corresponding to the label in certdata.txt.

# https://lists.ipfire.org/pipermail/development/2022-November/014681.html
"TrustCor RootCert CA-1"
"TrustCor RootCert CA-2"
"TrustCor ECA-1"
