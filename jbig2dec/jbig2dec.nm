###############################################################################
# IPFire.org    - An Open Source Firewall Solution                            #
# Copyright (C) - IPFire Development Team <info@ipfire.org>                   #
###############################################################################

name       = jbig2dec
version    = 0.19
release    = 1.1

groups     = System/Libraries
url        = https://jbig2dec.com
license    = AGPLv3+
summary    = A decoder implementation of the JBIG2 image compression format

description
	jbig2dec is a decoder implementation of the JBIG2 image compression format.
	JBIG2 is designed for lossy or lossless encoding of 'bilevel' (1-bit
	monochrome) images at moderately high resolution, and in particular scanned
	paper documents. In this domain it is very efficient, offering compression
	ratios on the order of 100:1.
end

source_dl  = https://github.com/ArtifexSoftware/ghostpdl-downloads/releases/download/gs9533/

build
	requires
		libpng-devel
		zlib-devel
	end
end

packages
	package %{name}

	package %{name}-libs
		template LIBS
	end

	package %{name}-devel
		template DEVEL

		requires
			jbig2dec-libs = %{thisver}
		end
	end

	package %{name}-debuginfo
		template DEBUGINFO
	end
end
