###############################################################################
# IPFire.org    - An Open Source Firewall Solution                            #
# Copyright (C) - IPFire Development Team <info@ipfire.org>                   #
###############################################################################

name       = valgrind
version    = 3.24.0
release    = 1

groups     = Development/Debuggers
url        = https://valgrind.org/
license    = GPL
summary    = Tool to help find memory-management problems in programs

description
	Valgrind is an instrumentation framework for building dynamic analysis
	tools. There are Valgrind tools that can automatically detect many
	memory management and threading bugs, and profile your programs in
	detail. You can also use Valgrind to build new tools.
end

source_dl  = https://sourceware.org/pub/valgrind/
sources    = %{thisapp}.tar.bz2

build
	requires
		autoconf
		automake
		docbook-dsssl
		docbook-xsl
		docbook-utils
		gdb
		perl
	end

	# valgrind cannot be compiled with stack protection enabled as it
	# implements its own functions.
	CFLAGS += -fno-stack-protector

	# The linker fails to find memcpy without this flag
	LTO_CFLAGS =

	prepare_cmds
		./autogen.sh
	end

	configure += \
		--enable-only64bit \
		--enable-lto=yes

	test
		make check
	end
end

packages
	package %{name}

	package %{name}-devel
		template DEVEL
	end

	package %{name}-debuginfo
		template DEBUGINFO
	end
end
