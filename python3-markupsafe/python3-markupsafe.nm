###############################################################################
# IPFire.org    - An Open Source Firewall Solution                            #
# Copyright (C) - IPFire Development Team <info@ipfire.org>                   #
###############################################################################

name       = python3-markupsafe
version    = 2.1.3
release    = 1

groups     = Development/Tools
url        = https://github.com/pallets/markupsafe
license    = BSD
summary    = Safely add untrusted strings to HTML/XML markup.

description
	MarkupSafe implements a text object that escapes characters so it is safe
	to use in HTML and XML. Characters that have special meanings are replaced
	so that they display as the actual characters. This mitigates injection
	attacks, meaning untrusted user input can safely be displayed on a page.
end

source_dl  = https://github.com/pallets/markupsafe/archive/refs/tags/%{version}.tar.gz#/

build
	requires
		python3-devel >= 3.4
		python3-setuptools
	end

	DIR_APP = %{DIR_SRC}/markupsafe-%{version}

	build
		%{python3} setup.py build
	end

	install
		%{python3} setup.py install --root=%{BUILDROOT} --skip-build
	end
end

packages
	package %{name}

	package %{name}-debuginfo
		template DEBUGINFO
	end
end
