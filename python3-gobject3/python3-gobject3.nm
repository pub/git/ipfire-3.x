###############################################################################
# IPFire.org    - An Open Source Firewall Solution                            #
# Copyright (C) - IPFire Development Team <info@ipfire.org>                   #
###############################################################################

name       = python3-gobject3
version    = %{ver_major}.%{ver_minor}
ver_major  = 3.46
ver_minor  = 0
release    = 1
thisapp    = pygobject-%{version}

groups     = Development/Tools
url        = https://www.pygtk.org/
license    = LGPLv2+
summary    = Python 2 bindings for GObject.

description
	The %{name} package provides a convenient wrapper for the GObject library
	for use in Python programs.
end


source_dl  = https://ftp.gnome.org/pub/GNOME/sources/pygobject/%{ver_major}/
sources    = %{thisapp}.tar.xz

build
	requires
		cairo-gobject-devel >= 1.12.12
		glib2-devel >= 2.34.2
		gobject-introspection-devel >= 1.56
		pkg-config
		python3-cairo-devel >= 1.16.0
		python3-devel >= 3.4
		python3-setuptools
	end

	build
		%{python3} setup.py build
	end

	install
		%{python3} setup.py install --skip-build --root=%{BUILDROOT}

		# Set correct header file permissions.
		find %{BUILDROOT}%{includedir} -type f -iname "*.h" \
			-exec chmod -v 644 {} \;
	end	
end

packages
	package %{name}
		requires
			gobject-introspection >= 1.56
			python3-cairo
		end
	end

	package %{name}-devel
		template DEVEL

		requires
			glib2-devel
			gobject-introspection-devel >= 1.56
		end
	end

	package %{name}-debuginfo
		template DEBUGINFO
	end
end
