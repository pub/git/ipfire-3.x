###############################################################################
# IPFire.org    - An Open Source Firewall Solution                            #
# Copyright (C) - IPFire Development Team <info@ipfire.org>                   #
###############################################################################

name       = cups
version    = 2.3.6
release    = 3

groups     = Applications/Printing
url        = https://cups.org/software.php
license    = GPLv2+
summary    = The common UNIX printing system

description
	CUPS is the standards-based, open source printing system developed
	by Apple Inc. for Mac OS(R) X and other UNIX(R)-like operating systems.
end

source_dl  = https://github.com/apple/cups/archive/refs/tags/v%{version}.tar.gz#/
sources    = %{thisapp}-source.tar.gz

build
	requires
		autoconf
		automake
		avahi-devel
		cyrus-sasl-devel
		dbus-devel
		gcc-c++
		gnutls-devel
		libacl-devel
		libgcrypt-devel
		libjpeg-devel
		libpng-devel
		libtasn1-devel
		libtiff-devel
		libusb-devel
		openldap-devel
		pam-devel
		systemd-devel
		zlib-devel
	end

	configure_options += \
		--localedir=%{datadir}/locale \
		--with-cupsd-file-perm=0755 \
		--with-log-file-perm=0600 \
		--with-docdir=%{datadir}/%{name}/www \
		--with-dbusdir=%{sysconfdir}/dbus-1 \
		--enable-debug \
		--enable-relro \
		--enable-avahi \
		--enable-threads \
		--enable-gnutls \
		--enable-webif \
		--with-xinetd=no

	make_install_targets += \
		BUILDROOT=%{BUILDROOT}

	install_cmds
		# Install default config file.
		mkdir -pv %{BUILDROOT}/etc/cups
		cp -vf %{DIR_APP}/conf/cupsd.conf %{BUILDROOT}/etc/cups/

		# Fix file permissions in /usr/include
		find %{BUILDROOT}%{includedir} -type f -iname "*.h" \
			-exec chmod 644 {} \;

		# Drop /var/run
		rm -rvf %{BUILDROOT}%{localstatedir}/run
	end
end

packages
	package %{name}
		recomends
			ghostscript
		end

		script postin
			systemctl daemon-reload >/dev/null 2>&1 || :
			systemctl --no-reload enable cups.path >/dev/null 2>&1 || :
		end

		script preun
			systemctl --no-reload disable cups.path >/dev/null 2>&1 || :
			systemctl --no-reload disable cupsd.socket >/dev/null 2>&1 || :
			systemctl --no-reload disable cupsd.service >/dev/null 2>&1 || :
			systemctl stop cupsd.socket >/dev/null 2>&1 || :
			systemctl stop cupsd.service >/dev/null 2>&1 || :
			systemctl stop cups.path >/dev/null 2>&1 || :
		end

		script postun
			systemctl daemon-reload >/dev/null 2>&1 || :
		end

		script postup
			systemctl daemon-reload >/dev/null 2>&1 || :
			systemctl try-restart cups.path >/dev/null 2>&1 || :
			systemctl try-restart cupsd.service >/dev/null 2>&1 || :
		end
	end

	package %{name}-client
		summary = CUPS printing system - client programs.
		description
			CUPS printing system provides a portable printing layer for
			UNIX operating systems. This package contains command-line client
			programs.
		end

		requires
			%{name}-libs = %{thisver}
		end

		provides += \
			lpr

		files
			%{sbindir}/lpc.cups
			%{bindir}/cancel*
			%{bindir}/lp*
			%{mandir}/man1/lp*.1.gz
			%{mandir}/man1/cancel-cups.1.gz
			%{mandir}/man8/lpc-cups.8.gz
		end
	end

	package %{name}-lpd
		summary = CUPS printing system - lpd emulation.
		description
			CUPS printing system provides a portable printing layer for
			UNIX operating systems. This is the package that provides standard
			lpd emulation.
		end

		requires
			%{name} = %{thisver}
			%{name}-libs = %{thisver}
		end

		provides += \
			lpd

		files
			%{unitdir}/cups-lpd.socket
			%{unitdir}/cups-lpd@.service
			%{prefix}/lib/daemon/cups-lpd
			%{mandir}/man8/cups-lpd.8.gz
		end

		script postin
			systemctl daemon-reload >/dev/null 2>&1 || :
		end

		script preun
			systemctl --no-reload disable cups-lpd.socket >/dev/null 2>&1 || :
			systemctl --no-reload disable cups-lpd@.service >/dev/null 2>&1 || :
			systemctl stop cups-lpd.socket >/dev/null 2>&1 || :
			systemctl stop cups-lpd@.service >/dev/null 2>&1 || :
		end

		script postun
			systemctl daemon-reload >/dev/null 2>&1 || :
		end

		script postup
			systemctl daemon-reload >/dev/null 2>&1 || :
			systemctl try-restart cups-lpd@.service >/dev/null 2>&1 || :
		end
	end

	package %{name}-devel
		template DEVEL

		requires += %{name}-libs=%{thisver}
	end

	package %{name}-libs
		template LIBS
	end

	package %{name}-ipptool
		summary = CUPS printing system - tool for performing IPP requests.
		description
			Sends IPP requests to the specified URI and tests and/or displays \
			the results.
		end

		requires
			%{name}-libs = %{thisver}
		end

		files
			%{bindir}/ipptool
			%{bindir}/ippfind
			%{datadir}/cups/ipptool
			%{datadir}/cups/ipptool/*
			%{mandir}/man1/ipptool.1.gz
			%{mandir}/man5/ipptoolfile.5.gz
		end
	end

	package %{name}-debuginfo
		template DEBUGINFO
	end
end
