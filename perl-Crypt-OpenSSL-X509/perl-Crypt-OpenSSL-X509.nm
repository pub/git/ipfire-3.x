###############################################################################
# IPFire.org    - An Open Source Firewall Solution                            #
# Copyright (C) - IPFire Development Team <info@ipfire.org>                   #
###############################################################################

name       = perl-Crypt-OpenSSL-X509
version    = 1.914
release    = 4
thisapp    = Crypt-OpenSSL-X509-%{version}

groups     = Development/Libraries
url        = http://search.cpan.org/dist/Crypt-OpenSSL-X509
license    = GPL+
summary    = Perl extension to OpenSSL's X509 API.

description
	This perl module implements a large majority of OpenSSL's useful X509 API.
end

source_dl  = https://cpan.metacpan.org/authors/id/J/JO/JONASBN/

build
	requires
		openssl-devel
		perl(Crypt::OpenSSL::Guess)
		perl(Convert::ASN1)
		perl(ExtUtils::MakeMaker)
		perl(Test::More)
	end

	build
		perl Makefile.PL INSTALLDIRS=vendor
		make %{PARALLELISMFLAGS}
	end

	make_install_targets = \
		pure_install

	test
		make test
	end

	install_cmds
		# Set correct library permissions.
		find %{BUILDROOT}%{libdir} -type f -iname "*.so" \
			-exec chmod 755 {} \;
	end
end

packages
	package %{name}

	package %{name}-debuginfo
		template DEBUGINFO
	end
end
