###############################################################################
# IPFire.org    - An Open Source Firewall Solution                            #
# Copyright (C) - IPFire Development Team <info@ipfire.org>                   #
###############################################################################

name       = python3-cairo
version    = 1.24.0
release    = 1
thisapp    = pycairo-%{version}

groups     = Development/Tools
url        = https://cairographics.org/pycairo
license    = MPLv1.1 or LGPLv2
summary    = Python3 bindings for the cairo library

description
	Python3 bindings for the cairo library.
end

source_dl  = https://github.com/pygobject/pycairo/releases/download/v%{version}/

build
	requires
		cairo-devel
		pkg-config
		python3-devel >= 3.4
		python3-setuptools
	end

	build
		%{python3} setup.py build
	end

	install
		%{python3} setup.py install --skip-build --root="%{BUILDROOT}"

		# Fix header permissions.
		find %{BUILDROOT}%{includedir} -type f -iname "*.h" \
			-exec chmod -v 644 {} \;
	end
end

packages
	package %{name}

	package %{name}-devel
		template DEVEL

		# The development headers need the cairo headers.
		requires
			cairo-devel
			python3-devel
		end
	end

	package %{name}-debuginfo
		template DEBUGINFO
	end
end
