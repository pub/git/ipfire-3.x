###############################################################################
# IPFire.org    - An Open Source Firewall Solution                            #
# Copyright (C) - IPFire Development Team <info@ipfire.org>                   #
###############################################################################

name       = libtiff
version    = 4.6.0
release    = 1
thisapp    = tiff-%{version}

groups     = System/Libraries
url        = https://libtiff.gitlab.io/libtiff/
license    = libtiff ( own )
summary    = Library of functions for manipulating TIFF format image files.

description
	The libtiff package contains a library of functions for manipulating
	TIFF (Tagged Image File Format) image format files.  TIFF is a widely
	used file format for bitmapped images.  TIFF files usually end in the
	.tif extension and they are often quite large.
end

source_dl  = https://download.osgeo.org/libtiff/

build
	requires
		gcc-c++
		libjpeg-devel
		zlib-devel
		zstd-devel
	end

	export LD_LIBRARY_PATH = %{DIR_APP}/libtiff/.libs

	configure_options += \
		--enable-defer-strile-load \
		--enable-chunky-strip-read

	test
		make check
	end
end

packages
	package %{name}

	package %{name}-devel
		template DEVEL
	end

	package %{name}-debuginfo
		template DEBUGINFO
	end
end
