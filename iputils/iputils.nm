###############################################################################
# IPFire.org    - An Open Source Firewall Solution                            #
# Copyright (C) - IPFire Development Team <info@ipfire.org>                   #
###############################################################################

name       = iputils
version    = 20221126
release    = 2

groups     = Networking/Tools
url        = https://www.skbuff.net/iputils
license    = BSD
summary    = Network monitoring tools including ping.

description
	The iputils package contains basic utilities for monitoring a network,
	including ping. The ping command sends a series of ICMP protocol
	ECHO_REQUEST packets to a specified network host to discover whether
	the target machine is alive and receiving network traffic.
end

source_dl  = https://github.com/iputils/iputils/archive/refs/tags/%{version}.tar.gz#/

build
	requires
		gettext-devel
		iproute2
		libcap-devel
		libidn2-devel
		libxslt
		meson
		ninja
		openssl-devel
		docbook-xsl <= 1.79.2
	end

	build
		%{meson}
		%{meson_build}
	end

	install
		%{meson_install}

		# Create ping6 as a symlink to ping
		ln -svf ping %{BUILDROOT}%{bindir}/ping6
		ln -svf ping.8 %{BUILDROOT}%{mandir}/man8/ping6.8

		ln -svf tracepath %{BUILDROOT}%{bindir}/tracepath6
		ln -svf tracepath.8 %{BUILDROOT}%{mandir}/man8/tracepath6.8

		# Set capabilities
		setcap cap_net_admin=ep %{BUILDROOT}%{bindir}/ping
		setcap cap_net_raw=ep %{BUILDROOT}%{bindir}/arping
	end
end

packages
	package %{name}
		provides
			/bin/arping
			/bin/ping
			/bin/ping6
		end
	end

	package %{name}-debuginfo
		template DEBUGINFO
	end
end
