###############################################################################
# IPFire.org    - An Open Source Firewall Solution                            #
# Copyright (C) - IPFire Development Team <info@ipfire.org>                   #
###############################################################################

name       = qemu
version    = 8.1.0
release    = 1

groups     = Applications/Virtualization
url        = https://www.qemu.org
license    = GPLv2+
summary    = QEMU is a generic and open source machine emulator and virtualizer

description
	QEMU is a processor emulator that relies on dynamic binary translation
	to achieve a reasonable speed while being easy to port on new host CPU
	architectures.
end

source_dl  = https://download.qemu.org/
sources    = %{thisapp}.tar.xz

build
	requires
		autoconf
		automake
		bison
		bzip2-devel
		cairo-devel
		flex
		glib2-devel
		glibc-devel >= 2.36
		gnutls-devel
		libcurl-devel
		libgcrypt-devel
		libjpeg-devel
		libpng-devel
		libseccomp-devel
		libtasn1-devel
		libudev-devel
		libusb-devel
		libuuid-devel
		lzo-devel
		meson
		ninja
		ncurses-devel
		nettle-devel
		pciutils-devel
		pixman-devel
		systemd-devel
		zlib-devel
		zstd-devel
	end

	targets = \
		aarch64-linux-user \
		aarch64-softmmu \
		riscv64-linux-user \
		riscv64-softmmu \
		x86_64-linux-user \
		x86_64-softmmu

	configure_options = \
		--prefix=%{prefix} \
		--bindir=%{bindir} \
		--sysconfdir=%{sysconfdir} \
		--libdir=%{libdir} \
		--libexecdir=%{libdir} \
		--datadir=%{datadir} \
		--mandir=%{mandir} \
		--localstatedir=%{localstatedir} \
		--extra-cflags="%{CFLAGS}" \
		--extra-ldflags="%{LDFLAGS}" \
		--disable-strip \
		--disable-werror \
		--target-list="%{targets}" \
		--enable-bzip2 \
		--enable-fdt \
		--enable-kvm \
		--enable-libusb \
		--enable-lzo \
		--enable-lto \
		--enable-pie \
		--enable-vnc \
		--enable-vnc-jpeg \
		--enable-seccomp \
		--disable-xen \
		--disable-dbus-display \
		--disable-sdl

	install_cmds
		# Remove ivshm stuff
		rm -vf %{BUILDROOT}%{bindir}/ivshmem* %{BUILDROOT}%{mandir}/ivshmem*

		# Remove deprecated run directory.
		rm -rvf %{BUILDROOT}/var/run

		# Remove firmware files for non supported platforms.
		rm -rvf %{BUILDROOT}%{datadir}/%{name}/hppa-firmware.img
		rm -rvf %{BUILDROOT}%{datadir}/%{name}/openbios-ppc
		rm -rvf %{BUILDROOT}%{datadir}/%{name}/openbios-sparc32
		rm -rvf %{BUILDROOT}%{datadir}/%{name}/openbios-sparc64
		rm -rvf %{BUILDROOT}%{datadir}/%{name}/palcode-clipper
		rm -rvf %{BUILDROOT}%{datadir}/%{name}/s390-ccw.img
		rm -rvf %{BUILDROOT}%{datadir}/%{name}/s390-netboot.img
		rm -rvf %{BUILDROOT}%{datadir}/%{name}/u-boot.e500

		# Make firmware non-executable
		find %{BUILDROOT}%{datadir}/%{name} -type f -executable | xargs chmod -v a-x
	end
end

packages
	package %{name}
		# Always install the native package (for KVM)
		requires
			qemu-%{DISTRO_ARCH} = %{thisver}
		end

		recommends
			qemu-img = %{thisver}
		end
	end

	template QEMUARCH
		summary = QEMU for %{qemu_arch}
		description = %{summary}

		requires = %{name} = %{thisver}

		files
			%{bindir}/qemu-%{qemu_arch}
			%{bindir}/qemu-system-%{qemu_arch}
			%{mandir}/man1/qemu-%{qemu_arch}.1*
			%{mandir}/man1/qemu-system-%{qemu_arch}.1*
		end
	end

	package %{name}-aarch64
		template QEMUARCH
		qemu_arch = aarch64
	end

	package %{name}-riscv64
		template QEMUARCH
		qemu_arch = riscv64
	end

	package %{name}-x86_64
		template QEMUARCH
		qemu_arch = x86_64
	end

	package %{name}-img
		summary = QEMU command line tool for manipulating disk images
		description
			This package provides a command line tool for manipulating disk images
		end

		files
			%{bindir}/qemu-img
			%{bindir}/qemu-io
			%{bindir}/qemu-nbd
			%{mandir}/man1/qemu-img.1*
			%{mandir}/man8/qemu-nbd.8*
		end
	end

	package %{name}-devel
		template DEVEL
	end

	package %{name}-debuginfo
		template DEBUGINFO
	end
end
